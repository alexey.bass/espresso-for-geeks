/* Copyright (c) 2017 Philippe Kalaf, MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


// brew worker period in ms 
#define BREW_WORKER_PERIOD 100

// Boiler PID worker in ms
#define PID_WORKER_PERIOD 500

// PWM period
// 0.8333 for 60Hz, 1 for 50Hz for 1% resolution
#define BOILER_PWM_PERIOD 0.8333

// Soft stop time
#define SOFT_STOP_TIME_S 7.0

// Steam timeout in seconds
#define STEAM_TIMEOUT 300.0

// Steam temperature
#define STEAM_TEMPERATURE 140.0

// Manage different brew modes and timings
#include "brewcontrol.h"

BrewControl::BrewControl(   PinName brew_pin, 
                            PinName flow_sensor_pin,
                            PinName zcd_input_pin,
                            PinName pump_control_pin,
                            PinName pressure_sensor_pin,
                            PinName temp_sensor_pin,
			    PinName temp2_sensor_pin,
                            PinName boiler_pwm_pin
                            ) : 
                            _brew_switch(brew_pin, 0), 
                            _flow_sensor(flow_sensor_pin), 
                            _pump_control(zcd_input_pin, pump_control_pin),
                            _pressure_sensor(pressure_sensor_pin),
                            _temp_sensor(temp_sensor_pin),
			    _temp2_sensor(temp2_sensor_pin),
                            _boiler_pwm(boiler_pwm_pin),
                            _brew_worker_thread(osPriorityNormal, 768),
                            _pid_worker_thread(osPriorityNormal, 256)
{
    _preinfuse_time = 0;
    _brew_switch = 0;

    // at 60Hz, we got 120 zero-crosses per sec, we want to capture 100 of 
    // those within each PWM period
    _boiler_pwm.period(BOILER_PWM_PERIOD);
    
    // let's start at 93 C
    _target_shot_temperature = 93;

    // 9 bars is default
    _target_shot_pressure = 9;

    // this is used for steam mode to return to prev target temp when done
    _prev_temp = 0;

    _boiler_pid.setPIDGains(0.075, 0.1, 0.9);
    _boiler_pid.setIntegratorLimits(0, 1);

    // Boiler is on by default
    enable_boiler();

    // Start main worker thread of brewing
    _brew_worker_thread.start(callback(this, &BrewControl::_brew_worker));

    // Start pid worker thread
    _pid_worker_thread.start(callback(this, &BrewControl::_boiler_pid_worker));

    _set_state(STOPPED);
}

float BrewControl::get_current_temperature_side()
{
    return _temp_sensor.read();
}

float BrewControl::get_current_temperature_top()
{
    return _temp2_sensor.read();
}

float BrewControl::get_current_temperature()
{
    // Let's return average of both sensors
    //return (_temp_sensor.read() + _temp2_sensor.read())/2;
    return _temp_sensor.read();
}

void BrewControl::set_shot_temperature(float shot_temp)
{
    _brew_worker_mutex.lock();

    if (shot_temp <= 20)
        _target_shot_temperature = 20;
    else if (shot_temp >= 150)
        _target_shot_temperature = 150;
    else
        _target_shot_temperature = shot_temp;

    _brew_worker_mutex.unlock();
}

float BrewControl::get_shot_temperature()
{
    return _target_shot_temperature;
}

void BrewControl::set_shot_pressure(float pressure)
{
    _brew_worker_mutex.lock();

    if (pressure <= 0)
        _target_shot_pressure = 0;
    else if (pressure >= 12)
        _target_shot_pressure = 12;
    else
        _target_shot_pressure = pressure;

    _brew_worker_mutex.unlock();
}

// Return pressure in bars
float BrewControl::get_current_pressure()
{
    return _pressure_sensor.read_bars();
}

int BrewControl::get_shot_volume()
{
    return _target_shot_volume;
}

void BrewControl::_boiler_pid_worker()
{
    while(true)
    {
        ThisThread::sleep_for(PID_WORKER_PERIOD);

        if(!_enable_boiler)
            continue;

        // take temperature measurement
        float latestTemp = get_current_temperature(); 

        float power = 0.0;

        // if the temperature is near zero, we assume there's an error
        if ( latestTemp > 0.5 ) {
            // calculate PID update
            power = _boiler_pid.update(_target_shot_temperature - latestTemp,
                    latestTemp);
        }

        // Validate number
        if ( power > 1 )
            power = 1;
        else if ( power < 0 )
            power = 0;

        // let's set new power on boiler
        _boiler_pwm = power;

        // store the latest temperature reading
        _latest_temp = latestTemp;
    }
}

void BrewControl::enable_boiler()
{
    _enable_boiler = 1;
}

void BrewControl::disable_boiler()
{
    _enable_boiler = 0;
    _boiler_pwm = 0;
}

bool BrewControl::toggle_boiler()
{
    if (_enable_boiler)
        disable_boiler();
    else
        enable_boiler();
    
    return _enable_boiler;
}

void BrewControl::pressure_up(uint8_t value)
{
    // limit to 12 bars
    if (get_current_pressure() <= 11.5)
        _pump_control.level_up(value);
}

void BrewControl::pressure_down(uint8_t value)
{
    _pump_control.level_down(value);
}

// pre-infuse time in seconds
// set to 0 to disable
void BrewControl::set_preinfuse_time(int time)
{
    _brew_worker_mutex.lock();

    if (time > 0)
        _preinfuse_time = time;
    else
        _preinfuse_time = 0;

    _brew_worker_mutex.unlock();
}

int BrewControl::get_preinfuse_time()
{
    return _preinfuse_time;
}

void BrewControl::stop_preinfuse_now()
{
    _stop_preinfuse = 1;
}

uint8_t BrewControl::get_pump_level()
{
    return _pump_control.get_level();
}

// This is to set the wanted shot time
void BrewControl::set_shot_time(int time)
{
    _brew_worker_mutex.lock();

    _target_shot_time = time;

    _brew_worker_mutex.unlock();
}

// This is to set the wanted shot volume
void BrewControl::set_shot_volume(int volume)
{
    _brew_worker_mutex.lock();

    if (volume >= 0)
        _target_shot_volume = volume;

    _brew_worker_mutex.unlock();
}

// This is to set the wanted flow rate (ml/s)
void BrewControl::set_shot_flow_rate(float flow_rate)
{
    _brew_worker_mutex.lock();

    _target_flow_rate = flow_rate;

    _brew_worker_mutex.unlock();
}

// return current shot_clock in seconds
float BrewControl::get_current_time()
{
    return _shot_clock;
}

// return current volume in ml
float BrewControl::get_current_volume()
{
    return _flow_sensor.get_volume();
}

// return the current flow rate
float BrewControl::get_current_flow_rate()
{
    return _flow_sensor.get_flow_rate();
}

// read brew on/off state
uint8_t BrewControl::get_state()
{
    return _state;
}

// Internal helper function to set brew states properly
void BrewControl::_set_state(uint8_t new_state)
{
    // Let's start the average pressure calculation when we start brewing
    if ((_state == STOPPED || _state == PRE_INFUSING) && new_state == BREWING)
        _pressure_sensor.start_count();
    // Let's stop the average pressure calculation when we stop brewing
    else if (_state == BREWING && (new_state == SOFT_STOPPING || new_state == STOPPED))
        _average_pressure = _pressure_sensor.stop_count();

    _state = new_state;
    if (_state == BREWING || _state == STEAMING || _state == PRE_INFUSING)
        _brew_worker_thread.flags_set(1);
}

void BrewControl::_brew_worker()
{
    while(true)
    {
        // If we are not brewing, let's just wait until we start brewing
        if (_state != PRE_INFUSING && _state != BREWING && _state != STEAMING)
        {
            // _set_state() will set the flag when we start brewing
            ThisThread::flags_wait_any(1);
        }
        
        _brew_worker_mutex.lock();

#ifdef LOG
        _log_brew_params();
#endif

        if (_state == PRE_INFUSING)
        {
            // First let's fill up the portafilter
            if (_flow_sensor.get_volume() <= PORTAFILTER_VOLUME && !_stop_preinfuse)
            {
                _brew_worker_mutex.unlock();
                continue;
            }

            // It's full, let's stop the pump for set pre-infuse time
            if (_pump_control.get_level() != 0)
            {
                _pump_control.set_level(0);
                _shot_clock.reset();
            }

            // Once pre-infuse time runs out, set brew mode
            if (_shot_clock.read() >= _preinfuse_time)
            {
                _mode = _prev_mode;
                _pump_control.set_level(60);
                _flow_sensor.reset_count();
                _shot_clock.reset();
                _set_state(BREWING);
                _stop_preinfuse = 0;
            }

            _brew_worker_mutex.unlock();
            continue;
        }

        if(_mode == MODE_TIME)
        {
            // Auto-adjust pressure to target
            float error = _target_shot_pressure - get_current_pressure();
            if(error < -0.25)
                pressure_down();
            else if(error > 0.25) 
                pressure_up();

            if(_shot_clock.read() >= _target_shot_time)
            {
                _brew_worker_mutex.unlock();
                soft_stop();
                _brew_worker_mutex.lock();
            }
        }
        else if(_mode == MODE_YIELD)
        {
            // Auto-adjust pressure to target
            float error = _target_shot_pressure - get_current_pressure();
            if(error < -0.25)
                pressure_down();
            else if(error > 0.25)
                pressure_up();

            if(_flow_sensor.get_volume() >= _target_shot_volume)
            {
                _brew_worker_mutex.unlock();
                soft_stop();
                _brew_worker_mutex.lock();
            }
        }
        else if(_mode == MODE_TIME_YIELD)
        {
            // Re-calculate target flowrate =
            // remaining volume / remaining time
            _target_flow_rate = (_target_shot_volume - _flow_sensor.get_volume())
                / (_target_shot_time - _shot_clock.read());
            // Auto-adjust flow-rate
            if(_target_flow_rate < 0)
                // oops! we have run out of time! go full power!
                _pump_control.set_level(100);
            else if(_target_flow_rate - _flow_sensor.get_flow_rate() < 0)
                pressure_down();
            else
                pressure_up();

            // Stop when target shot volume is reached
            if(_flow_sensor.get_volume() >= _target_shot_volume)
            {
                _brew_worker_mutex.unlock();
                soft_stop();
                _brew_worker_mutex.lock();
            }
        }
        else if(_mode == MODE_MANUAL)
        {
        }
        else if(_mode == MODE_STEAM)
        {
            // automatically stop steaming after STEAM_TIMEOUT
            if (_shot_clock.read() >= STEAM_TIMEOUT)
                _stop();
        }

        ThisThread::sleep_for(BREW_WORKER_PERIOD);
        _brew_worker_mutex.unlock();

    }
}

uint8_t BrewControl::start(uint8_t mode)
{
    _brew_worker_mutex.lock();

    // We are already brewing, return brewing mode
    if(_state == BREWING) 
    {
        _brew_worker_mutex.unlock();
        return _mode;
    }
    
    _mode = mode;

    // reset shot clock and flow sensor
    _flow_sensor.reset_count();
    _shot_clock.reset();
    _shot_clock.start();

    // Let's save settings before we set pre-infuse mode
    // only pre-infuse if set to > 0s and not in manual or steam mode
    if (_preinfuse_time && _mode != MODE_MANUAL && _mode != MODE_STEAM)
    {
        _prev_mode = _mode;
	// set pre-infuse mode
	_set_state(PRE_INFUSING);

	// we pre-infuse at low pressure
        _pump_control.set_level(60);
    }
    else
        _set_state(BREWING);

    if(_mode == MODE_STEAM)
    {
 	// save currently set temperature to return to it after steaming
	_prev_temp = get_shot_temperature();
	set_shot_temperature(STEAM_TEMPERATURE);
    }
    else
	// Open solenoid
	_brew_switch = 1;
    
    _brew_worker_mutex.unlock();
    
    return _mode;
}

void BrewControl::toggle_solenoid()
{
    _brew_worker_mutex.lock();

    if(_brew_switch)
	_brew_switch = 0;
    else
	_brew_switch = 1;

    _brew_worker_mutex.unlock();
}

// This function is sometimes called from an ISR
void BrewControl::_stop()
{
    _set_state(STOPPED);
    
    // Close solenoid
    _brew_switch = 0;

    // Stop and reset all counters and brew params
    _shot_clock.stop();
    _shot_clock.reset();
    _target_shot_volume = 0;
    _target_shot_time = 0;
    _flow_sensor.reset_count();

    // if we were in steam mode, _prev_temp will have been set
    if(_prev_temp)
    {
        _target_shot_temperature = _prev_temp;
	_prev_temp = 0;
    }
}

void BrewControl::soft_stop()
{
    _brew_worker_mutex.lock();

    // Stop immediately if in steam mode
    if (_mode == MODE_STEAM)
    {
	_stop();
        _brew_worker_mutex.unlock();
	return;
    }

    // Turn off pump
    _pump_control.set_level(0);

    // shot clock
    _shot_clock.stop();

    _set_state(SOFT_STOPPING);

    // Call stop() after SOFT_STOP_TIME_S
    _soft_stop_timer.attach(callback(this, &BrewControl::_stop),
			SOFT_STOP_TIME_S);

    _brew_worker_mutex.unlock();
}

float BrewControl::get_average_pressure()
{
    return _average_pressure;
}

uint8_t BrewControl::toggle(uint8_t mode)
{
    if(_state == BREWING || _state == PRE_INFUSING)
	soft_stop();
    else if(_state == STOPPED)
	start(mode);

    return _state;
}

PhaseControl *BrewControl::get_pump_control_ptr()
{
    return &_pump_control;
}

uint16_t BrewControl::get_last_pulse_count_side()
{
    return _temp_sensor.get_last_pulse_count();
}

uint16_t BrewControl::get_last_pulse_count_top()
{
    return _temp2_sensor.get_last_pulse_count();
}
